<?php

namespace app\controllers;

use app\models\Coche;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CocheController implements the CRUD actions for Coche model.
 */
class CocheController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Mostrar todos los coches con los botones de accion
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Coche::find(),
            'pagination' => [
                'pageSize' => 3
            ],
            'sort' => [
                'defaultOrder' => [
                    'bastidor' => SORT_ASC,
                ]
            ],
        ]);
        // llamo a la vista index y le paso el dataProvider
        //el dataProvider lo necesita el gridview
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Muestra los datos de un coche
     * @param string $bastidor Bastidor
     * @return string
     * @throws NotFoundHttpException si no encuentro el bastidor
     */
    public function actionView($bastidor)
    {
        return $this->render('view', [
            'model' => $this->findModel($bastidor),
        ]);
    }

    /**
     * Creates a new Coche model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Coche();

        if ($this->request->isPost) { //compruebo si he pulsado el boton y vengo del formulario
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'bastidor' => $model->bastidor]);
            }
        } else {
            $model->loadDefaultValues(); //tengo que colocar en el modelo los valores por defecto
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Actualiza los datos del bastidor del coche
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $bastidor Bastidor
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($bastidor)
    {
        $model = $this->findModel($bastidor); //te crea un modelo con los datos del coche que quieres cambiar

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'bastidor' => $model->bastidor]); //Redirecciona la pagina a otra
        }

        return $this->render('update', [ //muestra el formulario para actualizar los datos
            'model' => $model,
        ]);
    }

    /**
     * Elimina un coche
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $bastidor Bastidor
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($bastidor)
    {
        $this->findModel($bastidor)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Coche model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $bastidor Bastidor
     * @return Coche the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($bastidor)
    {
        if (($model = Coche::findOne(['bastidor' => $bastidor])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('No tenemos ese recurso');
    }
    public function actionMostrar()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Coche::find()
        ]);
        return $this->render(
            'mostrar', //nombre de la vista
            ['dataProvider' => $dataProvider] // datos a pasar a la vista
        );
    }
}
